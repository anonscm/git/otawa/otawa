# OTAWA

## Current Contributors

* [Hugues Casse](mailto:hugues.casse@irit.fr) (project manager)
* [Christine Rochange](mailto:christine.rochange@irit.fr)
* [Pascal Sainrat](mailto:pascal.sainrat@irit.fr)


## Old Contributors

* Zhenyu Bai
* Jordy Ruiz
* Haluk Ozaktas
* Clément Ballabriga
* Gabriel Cavaignac
* Frédéric Duloum
* Benoit Mathieu
* Fadia Nemer
* Melhem Tawk


## Contact

Website:
	http://www.otawa.fr

Email:
	[otawa@irit.fr](mailto:otawa@irit.fr)

Address:
	IRIT - Université de Toulouse 3
	118 route de Narbonne
	31062 Toulouse Cedex 4 France
	Tel.: 05 61 55 67 65
