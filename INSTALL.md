# Installing OTAWA

## Dependencies

Tools:
  * Bison
  * CMake
  * Flex
  * G++
  * LibXML2
  * LibXSLT

Optional tool:
  * Doxygen
  * GraphViz
  * Thot
  * CPlex

Internal dependencies (that must be compiled at the same level than **OTAWA** directory):
  * GEL
  * GELPP
  * ELM (compiled with XOM extension)


## Installation

To configure the make file, type:

```sh
cmake . -DCMAKE_INSTALL_PREFIX=path_to_install_to
```

To compile OTAWA, just:

```sh
make all
```

To install it:
```sh
make install
```


## Installation with Options

To use CPlex, pass the following option at CMake invocation:

```sh
cmake . -DCMAKE_INSTALL_PREFIX=path_to_install_to -DCPLEX_ROOT=<cplex root>
```

where <cplex root> is the top-level directory of CPlex. This directory should contains at least `cplex` and `concert` subirectories.

With **OTAWA v2**, some code (plugins, tools, etc) has been categorized as legacy code: they contains old algorithms that are no more used in the main stream of analysis. They are kept because (1) they may be compared to more modern approaches and (2) they may be expand by future research works. As a default, they are not compiled with the main stream **OTAWA** but they may be reactived
by defining the constant `USE_LEGACY`:

```sh
cmake . -DCMAKE_INSTALL_PREFIX=path_to_install_to -DUSE_LEGACY=YES
```

**OTAWA v2* introduces also parallel execution of analyses. As this feature is experimental for now, it is not enabled
as a default. To enable it, just type:

```sh
cmake . -DOTAWA_CONC=ON
```
