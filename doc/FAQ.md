# OTAWA's FAQ

## owcet/script



* How to display the computed ILP System to HTML file?

Add the following command after the step `otawa::ipet::WCET_FEATURE`:
```
<step processor="otawa::display::ILPSystemDisplayer"/>
```

The generated file will be _TASKNAME_`-ilp.html`.



* How to dump the computed ILP System?

Add the following command after the step `otawa::ipet::WCET_FEATURE`:
```
<step require="otawa::ilp::OUTPUT_FEATURE">
	<config name="otawa::ilp::OUTPUT_PATH" value="PATH"/>
</step>
```

The generated file will be _PATH_.



* How to get meaninful ILP variable names?

Put at the script global level, the command:
```
<config name="otawa::ilp::EXPLICIT" value="true"/>
```
