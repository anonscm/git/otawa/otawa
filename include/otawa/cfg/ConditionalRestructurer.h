/*
 *	ConditionalRestructurer class interface
 *
 *	This file is part of OTAWA
 *	Copyright (c) 2016, IRIT UPS.
 *
 *	OTAWA is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	OTAWA is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OTAWA; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef OTAWA_CFG_CONDITIONALRESTRUCTURER_H_
#define OTAWA_CFG_CONDITIONALRESTRUCTURER_H_

#include "CFGTransformer.h"
#include "features.h"

namespace otawa {

class ConditionalRestructurer: public CFGTransformer {
public:
	static p::declare reg;
	ConditionalRestructurer(p::declare& r = reg);

protected:
	void transform(CFG *g, CFGMaker &m) override;

private:

	class CondSeq {
	public:
		typedef enum {
			DISABLED,
			NO_MATCH,
			MATCH
		} match_t;

		inline CondSeq() {}
		CondSeq(Inst *i);
		match_t match(Inst *i);
		void write(const RegSet& rs);
		void getCases(Condition cs[3]) const;

		inline bool worths() const { return _part != 0 && icnt > 1; }
		inline Inst *inst() const { return _inst; }
		inline hard::Register *reg() const { return _reg; }
		inline int count() const { return icnt; }
		inline int part() const { return _part; }

	private:
		Inst *_inst;			// instruction generating the condition
		hard::Register *_reg;	// SR
		int icnt;				// controlled instruction count
		t::uint8 _part;			// condition
		bool _ena,				// condition enabled
			_unsigned;			// condition on unsigned value
	};

	class CondBB {
	public:
		CondBB();
		CondBB *duplicate() const;
		void add(Inst *i, t::uint b, bool is_control);
		Vector<Condition::cond_t> conds;	// current conditions
		Vector<Inst *> insts;				// instructions in this block
		t::uint8 branch;					// direction of condition (TAKEN, NOT_TAKEN, BOTH)
	} case_t;

	void split(Block *bb);
	Inst *nop(Inst *i, const Condition& cond = Condition());
	Inst *guard(Inst *i, const Condition& cond);
	Inst *cond(Inst *i);
	void link(Block *bb);
	bool useful(BasicBlock::InstIter i, const Condition& cond);
	void buildStatus(BasicBlock *bb, Vector<CondSeq>& status);
	void buildCondBB(BasicBlock *bb, const Vector<CondSeq>& seqs, Vector<CondBB *>& cbbs);

	Inst *_nop, *_anop;
};

} // otawa

#endif /* OTAWA_CFG_CONDITIONALRESTRUCTURER_H_ */
