/*
 *	$Id$
 *	ContextualLoopBound class interface
 *
 *	This file is part of OTAWA
 *	Copyright (c) 2008, IRIT UPS.
 *
 *	OTAWA is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	OTAWA is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OTAWA; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef OTAWA_FLOWFACT_FEATURES_H_
#define OTAWA_FLOWFACT_FEATURES_H_

#include <elm/xom/Element.h>

#include <otawa/dfa/State.h>
#include <otawa/proc/AbstractFeature.h>
#include <otawa/prog/Inst.h>
#include "../prop.h"

namespace otawa {

// flow fact configuration
extern p::id<Path> FLOW_FACTS_PATH;
extern p::id<xom::Element *> FLOW_FACTS_NODES;
extern p::id<bool> FLOW_FACTS_MANDATORY;

// features
extern p::feature FLOW_FACTS_FEATURE;
extern p::feature MKFF_PRESERVATION_FEATURE;

// control flow facts
extern p::id<Address> BRANCH_TARGET;
extern p::id<Address> CALL_TARGET;
extern p::id<bool> IGNORE_CONTROL;
extern p::id<bool> IGNORE_ENTRY;
extern p::id<bool> IGNORE_SEQ;
extern p::id<bool> IS_RETURN;
extern p::id<bool> NO_CALL;
extern p::id<bool> NO_RETURN;

extern p::id<Inst::kind_t> ALT_KIND;

// loop bounds
extern p::id<int> MAX_ITERATION;
extern p::id<int> MIN_ITERATION;
extern p::id<int> TOTAL_ITERATION;

// recursive bound
extern p::id<int> RECURSE_BOUND;

// virtualization control
extern p::id<bool> NO_INLINE;
extern p::id<bool> INLINING_POLICY;

// memory facts
extern p::id<Pair<Address, Address> > ACCESS_RANGE;

// state facts
extern p::id<dfa::State*> PROVIDED_STATE;
extern p::id<bool> EXIST_PROVIDED_STATE;

// flow fact administration
extern p::id<bool> PRESERVED;

} // otawa

#endif /* OTAWA_FLOWFACT_FEATURES_H_ */
