/*
 *	featrues for proc module
 *
 *	This file is part of OTAWA
 *	Copyright (c) 2024, IRIT UPS.
 *
 *	OTAWA is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	OTAWA is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OTAWA; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef OTAWA_PROC_FEATURES_H
#define OTAWA_PROC_FEATURES_H

#include <otawa/proc/AbstractFeature.h>
#include <otawa/proc/Processor.h>

namespace otawa {

class GlobStatsInterface {
public:
	virtual ~GlobStatsInterface();
	virtual io::Output& out() = 0;
	virtual bool doesGlobalStats(Processor& proc) const = 0;
};

extern p::interfaced_feature<GlobStatsInterface> GLOBAL_STAT_FEATURE;
extern p::id<bool> GLOBAL_STATS;
extern p::id<string> GLOBAL_STATS_FOR;
extern p::id<string> GLOBAL_STATS_TO;

}	// otawa

#endif	/* OTAWA_PROC_FEATURES_H */
