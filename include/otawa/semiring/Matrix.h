/*
 *	otawa::math::Matrix definition
 *
 *	This file is part of OTAWA
 *	Copyright (c) 2024, IRIT UPS.
 *
 *	OTAWA is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	OTAWA is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OTAWA; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef OTAWA_SEMIRING_MATRIX_H
#define OTAWA_SEMIRING_MATRIX_H

#include <elm/array.h>
#include <elm/assert.h>
#include <elm/types.h>

namespace otawa { namespace semiring {

using namespace elm;

template <class T>
class DefaultSemiring {
public:
	inline T zero() const { return 0; }
	inline bool isZero(const T& x) const { return x == 0; }
	inline T add(const T& x, const T& y) const { return x + y; }
	inline T mul(const T& x, const T& y) const { return x * y; }
};

template <class T, class S = DefaultSemiring<T> >
class Vector {
public:

	inline Vector(unsigned size, const T& init, const S& sr)
		: _size(size), _data(new T[size]), _sr(sr)
		{ array::set(_data, _size, init); }
	inline Vector(unsigned size): Vector(size, single<S>().zero(), single<S>()) {}
	inline Vector(unsigned size, const S& sr): Vector(size, single<S>().zero(), sr) {}
	inline Vector(unsigned size, const T& init): Vector(size, init, single<S>()) {}
	inline Vector(const Vector<T, S>& v)
		: _size(v._size), _data(new T[v._size]), _sr(v._sr)
		{ copy(v); }
	inline ~Vector() { delete [] _data; }

	inline const T& get(int i) const { ASSERT(0 <= i && i < _size); return _data[i]; }
	inline T& get(int i) { ASSERT(0 <= i && i < _size); return _data[i]; }

	inline void copy(const Vector<T, S>& v) {
		ASSERT(_size == v._size);
		array::copy(_data, v._data, _size);
	}

	inline void add(const Vector<T, S>& v) {
		ASSERT(_size == v._size);
		for(auto i = 0U; i < _size; i++) _data[i] = _sr.add(_data[i], v._data[i]);
	}

	inline T mul(const Vector<T, S>& v) const {
		ASSERT(_size == v._size);
		T sum = _sr.zero();
		for(auto i = 0U; i < _size; i++)
			_sr.add(sum, _sr.mul(_data[i], v._data[i]));
	}

private:
	unsigned _size;
	T *_data;
	const S& _sr;
};

template <class T, class S = DefaultSemiring<T> >
class Matrix {
public:
	inline Matrix(unsigned N, unsigned M, const T& init, const S& sr)
		: _N(N), _M(M), _data(new T[N * M]), _sr(sr)
		{ array::set(_data, N * M, init); }
	inline Matrix(unsigned N, unsigned M, const T& init): Matrix(N, M, init, single<S>()) {}
	inline Matrix(unsigned N, unsigned M, const S& sr): Matrix(N, M, single<S>().zero(), sr) {}
	inline Matrix(unsigned N, unsigned M): Matrix(N, M, single<S>().zero(), single<S>()) {}
	inline Matrix(const Matrix<T, S>& m)
		: _N(m._N), _M(m._M), _data(new T[m._N * m._M]), _sr(m._sr)
		{ copy(m); }
	inline ~Matrix() { delete _data; }

	inline const T& get(unsigned i, unsigned j) const { ASSERT(i < _N && j < _M); return _data[at(i, j)]; }
	inline T& get(unsigned i, unsigned j) { ASSERT(i < _N && j < _M); return _data[at(i, j)]; }

	inline void copy(const Matrix<T, S>& v) {
		ASSERT(_N == v._N && _M == v._M);
		array::copy(_data, v._data, _N * _M);
	}

	inline void add(const Matrix<T, S>& m) {
		ASSERT(_N == m._N && _M == m._M);
		for(int i = 0; i < _N * _M; i++)
			_data[i] = _sr.add(_data[i], m._data[i]);
	}

	inline void mult(const Matrix<T, S>& a, const Matrix<T, S>& b) {
		ASSERT(a._M == b._N && a._N == _N && b._M == _M);
		auto K = a._M;
		for(int i = 0; i < _N; i++)
			for(int j = 0; j < _M; j++) {
				T s = _sr.zero();
				for(int k = 0; k < K; k++)
					s = _sr.add(s, _sr.mul(a.get(i, k), b.get(k, j)));
				_data[at(i, j)] = s;
			}
	}

private:
	inline unsigned at(unsigned i, unsigned j) const { return i * _N + j; }
	unsigned _N, _M;
	T *_data;
	const S& _sr;
};

}}	// otawa:semiring

#endif	// OTAWA_SEMIRING_MATRIX_H
