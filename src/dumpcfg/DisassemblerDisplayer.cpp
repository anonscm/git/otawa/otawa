/*
 *	DisassembleDisplayer implementation
 *
 *	This file is part of OTAWA
 *	Copyright (c) 2016, IRIT UPS.
 *
 *	OTAWA is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	OTAWA is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OTAWA; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <elm/io.h>
#include <otawa/cfg/features.h>
#include "DisassemblerDisplayer.h"

using namespace elm;
using namespace otawa;

///
p::declare DisassemblerDisplayer::reg =
	p::init("DisassemblerDisplayer", Version(2, 1, 0))
	.extend<Displayer>()
	.make<DisassemblerDisplayer>();

/**
 */
DisassemblerDisplayer::DisassemblerDisplayer(p::declare& r): Displayer(r) {
}


/**
 */
void DisassemblerDisplayer::processWorkSpace(WorkSpace *ws) {
	const CFGCollection& coll = **otawa::INVOLVED_CFGS(ws);

	// process CFGs
	for(int i = 0; i < coll.count(); i++) {
		if(!display_all && i > 0)
			break;

		// display function head
		CFG *cfg = coll[i];
		cout << "# Function " << cfg->label() << '\n';

		// display blocks
		for(CFG::BlockIter v = cfg->blocks(); v(); v++) {

			// display header
			cout << "BB " << v->index() << ": ";
			for(Block::EdgeIter e = v->outs(); e(); e++) {
				if(e->sink()->isSynth())
					cout << " C(" << e->sink()->toSynth()->callee()->label() << ")";
				else {
					if(v->isEntry() || e->sink()->isExit())
						cout << " V(";
					else if(e->isNotTaken())
						cout << " NT(";
					else
						cout << " T(";
					cout << e->sink()->index() << ")";
				}
			}
			cout << io::endl;

			// if needed, display code
			if(v->isBasic()) {
				BasicBlock *bb = **v;
				for(BasicBlock::InstIter i(bb); i(); i++) {

					// Put the label
					for(Identifier<String>::Getter label(*i, FUNCTION_LABEL); label(); label++)
						cout << '\t' << *label << ":\n";
					for(Identifier<String>::Getter label(*i, LABEL); label(); label++)
						cout << '\t' << *label << ":\n";

					// Disassemble the instruction
					cout << "\t\t" << ot::address(i->address()) << ' ';
					i->dump(cout);
					cout << '\n';

				}
			}
		}
	}

}
