/*
 *	$Id$
 *	Copyright (c) 2005, IRIT UPS.
 *
 *	src/dumpcfg/SimpleDisplayer.h -- SimpleDisplayer class interface.
 */
#ifndef OTAWA_DUMPCFG_SIMPLE_DISPLAYER_H
#define OTAWA_DUMPCFG_SIMPLE_DISPLAYER_H

#include "Displayer.h"

// SimpleDisplayer class
class SimpleDisplayer: public Displayer {
public:
	static p::declare reg;
	SimpleDisplayer(p::declare& r = reg);
protected:
	void processWorkSpace(WorkSpace *ws) override;
private:
	int offset(CFG *cfg);
	static Identifier<int> OFFSET;
	int cfg_cnt;
};

#endif	// OTAWA_DUMPCFG_SIMPLE_DISPLAYER_H

