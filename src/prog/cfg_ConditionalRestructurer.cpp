/*
 *	ConditionalRestructurer class implementation
 *
 *	This file is part of OTAWA
 *	Copyright (c) 2016, IRIT UPS.
 *
 *	OTAWA is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	OTAWA is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OTAWA; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <otawa/cfg/ConditionalRestructurer.h>
#include <otawa/prog/VirtualInst.h>

#define DO_DEBUG

namespace otawa {

/// Flags which branch edges needs to be generated.
static const t::uint8
	NONE		= 0b00,		// no branch edge generated
	TAKEN		= 0b01,		// taken branch edge generated
	NOT_TAKEN	= 0b10,		// not-taken branch edge generated
	BOTH		= 0b11;		// taken and not-taken branch edges generated

/*
 * ConditionalRestructure provides a minimal implementation to support efficiently
 * and easily the conditional instruction. Yet, it remains space for improvement:
 *
 * * when a condition is refining a current condition, it would be enough to apply
 *   the refined condition to the initial assertion (as it will separated according to the
 *   new refinement),
 *
 * * the current system supports only condition of the semantic instructions (that are
 *   the main user of this module); yet, even non-standard condition could be supported
 *   as the condition system needs only to create an inclusion order on the conditions
 *   and the possibly to refine a condition getting the refinement and the complement
 *   of the refinement relatively to the current condition.
 */


/**
 * @class ConditionalRestructurer::CondBB
 * Class containing a version of BB according to some condition.
 */

///
ConditionalRestructurer::CondBB::CondBB(): branch(BOTH) {}

/**
 * Duplicate the block (because of condition change)
 */
ConditionalRestructurer::CondBB *ConditionalRestructurer::CondBB::duplicate() const {
	auto r = new CondBB;
	r->conds = conds;
	r->insts = insts;
	r->branch = branch;
	return r;
}

/**
 * Add an instruction to the block.
 */
void ConditionalRestructurer::CondBB::add(Inst *i, t::uint b, bool is_control) {
	insts.add(i);
	if(is_control)
		branch = b;
}


/**
 * @class ConditionalRestructurer::Status
 * Represents a condition status that is a condition and its context.
 */

/**
	* Buid a new condition status.
	* @param i		Instruction computing the condition.
	*/
ConditionalRestructurer::CondSeq::CondSeq(Inst *i): _inst(i), icnt(1), _ena(true) {
	auto c = i->condition();
	_reg = c.reg();
	_unsigned = c.isUnsigned();
	_part = c.cond();
}

/**
 * Filter the current condition with the guard condition from the passed
 * instruction.
 * @param i		Instruction to match with.
 * @return		DISABLED	SR matches but the condition is disabled
 * 				NO_MATCH	different SR
 * 				MATCH		Filter applied on the current condition from the instruction guard condition.
 */
ConditionalRestructurer::CondSeq::match_t ConditionalRestructurer::CondSeq::match(Inst *i) {
	auto ic = i->condition();
	if(ic.reg() != _reg)	// not same SR
		return NO_MATCH;
	if(!_ena)				// this SR has been overwritten
		return DISABLED;
	icnt++;
	if(_part != 0) {		// tracking disabled
		if(ic.isUnsigned() != _unsigned || ic.isAny()) {	// different sign: disabled
			_part = 0;
			icnt = 0;
		}
		else if(ic.cond() != _part						// not same
		&& (ic.cond() != (~_part & Condition::ANY)))	// not complement
			_part = Condition::ANY;						// => complex sequence
	}
	return MATCH;
}

/**
 * Record a write to registers and possibly to the SR. Disabled the status
 * if this is the case.
 * @param rs	Written registers.
 */
void ConditionalRestructurer::CondSeq::write(const RegSet& rs) {
	if(_ena && rs.contains(_reg->platformNumber()))
		_ena = false;
}

/**
 * @fn bool ConditionalRestructurer::Status::worths() const;
 * Test if it worth using this status to duplicate a BB (applying to more
 * than 1 instruction).
 */

/**
 * Build the cases where this conditions applies.
 * @param cs	To store cases as output.
 */
void ConditionalRestructurer::CondSeq::getCases(Condition cs[3]) const {
	if(_part == Condition::ANY) {
		cs[0] = Condition(_unsigned, Condition::EQ, _reg);
		cs[1] = Condition(_unsigned, Condition::LT, _reg);
		cs[2] = Condition(_unsigned, Condition::GT, _reg);
	}
	else {
		cs[0] = Condition(_unsigned, _part, _reg);
		cs[1] = Condition(_unsigned, (~_part) & Condition::ANY, _reg);
		cs[2] = Condition();
	}
}


/**
 * Class representing an instruction turned into a NOP but providing an "assume".
 */
class GuardNOP: public NOP {
public:
	GuardNOP(WorkSpace *ws, Inst *i, const Condition& cond)
		: NOP(ws, i), _cond(cond) { }

	int semInsts(sem::Block &block, int t) override {
		if(!_cond.isEmpty())
			block.add(sem::assume(_cond.semCond(), _cond.reg()->platformNumber()));
		return t;
	}

#	ifdef DO_DEBUG
	void dump(io::Output &out) override {
		if(!_cond.isEmpty())
			out << "(G)  ";
		else
			out << "(C) ";
		NOP::dump(out);
	}
#	endif

private:
	Condition _cond;
};


/**
 * Class representing a conditional instruction expressing the condition with an
 * "assume" and the semantic block without condition.
 */
class GuardInst: public VirtualInst {
public:
	GuardInst(WorkSpace *ws, Inst *i, const Condition& cond): VirtualInst(ws, i), _cond(cond) { }

	int semInsts(sem::Block &block, int t) override {
		if(!_cond.isEmpty())
			block.add(sem::assume(_cond.semCond(), _cond.reg()->platformNumber()));
		return VirtualInst::semKernel(block, t);
	}

#	ifdef DO_DEBUG
	void dump(io::Output &out) override {
		if(!_cond.isEmpty())
			out << "(G)  ";
		else
			out << "(C) ";
		VirtualInst::dump(out);
	}
#	endif

private:
	Condition _cond;
};

/**
 * Class representing a conditional instruction executed in a context corresponding
 * to its condition and therefore that does not need to expression the condition.
 */
class CondInst: public VirtualInst {
public:
	CondInst(WorkSpace *ws, Inst *i): VirtualInst(ws, i) { }

	int semInsts(sem::Block& block, int t) override {
		return VirtualInst::semKernel(block, t);
	}

#	ifdef DO_DEBUG
	void dump(io::Output &out) override {
		out << "(C)  ";
		VirtualInst::dump(out);
	}
#	endif

};


/**
 * Attached to a block to represent the different versions.
 * The pair is made of a new basic block and a branch mode
 * (one of NONE, TAKEN, NOT_TAKEN or BOTH).
 */
static p::id<Pair<Block *, int> > BB("");


/**
 * In the case where a loop header has to be duplicated, it
 * is first replaced by a virtual node (maintaining the simple)
 * loop structure that is tied to the conditional versions of
 * the header.
 */
static p::id<Block *> HD("");

/**
 * This feature ensures that the CFG is transformed to reflect the effects of conditional instructions.
 *
 * Default implementation:
 * @li @ref ConditionalRestructurer
 */
p::feature CONDITIONAL_RESTRUCTURED_FEATURE("otawa::CONDITIONAL_RESTRUCTURED_FEATURE", p::make<ConditionalRestructurer>());


/**
 */
p::declare ConditionalRestructurer::reg = p::init("otawa::ConditionalRestructurer", Version(2, 0, 0))
	.use(LOOP_HEADERS_FEATURE)
	.require(VIRTUAL_INST_FEATURE)
	.provide(CONDITIONAL_RESTRUCTURED_FEATURE)
	.extend<CFGTransformer>()
	.make<ConditionalRestructurer>();


/**
 */
ConditionalRestructurer::ConditionalRestructurer(p::declare& r)
: CFGTransformer(r), _nop(nullptr), _anop(nullptr) {
}


/**
 */
void ConditionalRestructurer::transform(CFG *g, CFGMaker &m) {

	// split the basic blocks
	for(CFG::BlockIter b(g->blocks()); b(); b++)
		split(*b);
	BB(g->entry()) = pair(m.entry(), int(BOTH));
	BB(g->exit()) = pair(m.exit(), int(BOTH));
	if(g->unknown())
		BB(g->unknown()) = pair(m.unknown(), int(BOTH));

	// re-build the CFG
	for(CFG::BlockIter b(g->blocks()); b(); b++)
		link(*b);
}


/**
 * Build the sequence of instruction condition on the same status register.
 * @param bb		BB to look sequence in.
 * @param status	To store the found sequences.
 */
void ConditionalRestructurer::buildStatus(BasicBlock *bb, Vector<CondSeq>& status) {
	RegSet wr;
	for(auto inst: *bb) {
		auto c = inst->condition();
		if(!c.isEmpty()) {

			// find corresponding status
			int p = status.length() - 1;
			bool matched = false;
			while(p >= 0)
				switch(status[p].match(inst)) {
				case CondSeq::MATCH:
					matched = true;
				case CondSeq::DISABLED:
					p = -1;
					break;
				case CondSeq::NO_MATCH:
					p = p - 1;
					break;
				}

			// or create a new one
			if(!matched)
				status.add(CondSeq(inst));

		}

		// update status according to writes
		wr.clear();
		inst->writeRegSet(wr);
		for(auto& s: status)
			s.write(wr);
	}
}


/**
 * Build conditinal BBs.
 * @param cbbs	To store built conditional BBs.
 */
void ConditionalRestructurer::buildCondBB(BasicBlock *bb, const Vector<CondSeq>& seqs, Vector<CondBB *>& cbbs) {
	cbbs.add(new CondBB);		// add initial case
	int cc = 0;					// current condition index to status

	for(auto inst: *bb) {
		auto icond = inst->condition();

		// new condition sequence
		if(cc < seqs.length() && seqs[cc].inst() == inst) {

			// not worth it: no reason to exploit it
			if(!seqs[cc].worths()) {
				for(auto c: cbbs) {
					c->conds.add(Condition().cond());
					c->add(inst, BOTH, inst->isControl());
				}
			}

			// worth it: duplicate the conditional BB according to condition
			else {
				Condition conds[3];
				seqs[cc].getCases(conds);
				auto clen = cbbs.length();

				// for each sub-condition
				for(int i = 0; i < 3; i++) {
					if(conds[i].isEmpty())
						continue;

					// build the guarded instruction
					auto mcond = icond & conds[i];
					Inst *added_inst;
					t::uint8 branch = BOTH;
					if(!mcond.isEmpty()) {
						added_inst = guard(inst, mcond);			// add guarded instruction
						if(inst->isControl())
							branch = TAKEN;
					}
					else {
						added_inst = nop(inst, ~mcond & conds[i]);	// add guarded nop
						if(inst->isControl())
							branch = NOT_TAKEN;
					}

					// duplicate the BBs
					int offset = 0;
					if(i >= 1) {
						offset = cbbs.length();
						for(int j = 0; j < clen; j++) {
							cbbs.add(cbbs[j]->duplicate());
							cbbs.top()->conds.pop();
							cbbs.top()->insts.pop();
						}
					}

					// add condition and guard
					for(int j = 0; j < clen; j++) {
						cbbs[j + offset]->conds.add(conds[i].cond());
						cbbs[j + offset]->add(added_inst, branch, false);
					}
				}
			}
			cc++;
		}

		// not a conditional instruction: add it to all sequences
		else if(icond.isEmpty())
			for(auto c: cbbs)
				c->insts.add(inst);

		// conditional instruction: sometimes add inst, sometimes NOP
		else {
			auto c = inst->condition();

			// find condition
			int i = cc - 1;
			while(seqs[i].reg() != icond.reg())
				i--;
			ASSERT(i >= 0);

			// not worth it: just add as is to all
			if(!seqs[i].worths()) {
				for(auto ca: cbbs)
					ca->add(inst, BOTH, inst->isControl());
			}

			// worth it: add the instruction and its corresponding NOP
			else {
				auto ninst = cond(inst);
				auto nop = new NOP(workspace(), inst);
				for(auto ca: cbbs)
					if(c.cond() & ca->conds[i])
						ca->add(ninst, TAKEN, inst->isControl());
					else
						ca->add(nop, NOT_TAKEN, inst->isControl());
			}
		}
	}


}



/**
 * Split the block according to the conditions.
 * Alternatives of the block are stored on the block itself using
 * BB identifier.
 *
 * @param b	Split block.
 */
void ConditionalRestructurer::split(Block *b) {

	// throw up non BB
	if(b->isEnd())
		return;
	if(b->isSynth()) {
		Block *cb = build(b->toSynth()->callee());
		BB(b) = pair(cb, int(BOTH));
		return;
	}
	if(!b->isBasic())
        return;
	auto bb = b->toBasic();
	if(logFor(LOG_BB))
		log << "\t\tsplitting " << b << io::endl;

	// build the conditional sequences
	Vector<CondSeq> seqs;
	buildStatus(bb, seqs);

	// buld conditional BBs
	Vector<CondBB *> cbbs;		// found cases
	buildCondBB(bb, seqs, cbbs);

	// header block with several cases: build a phony edge to avoid duplicate heads
	Block *h = nullptr;
	if(LOOP_HEADER(b) && cbbs.length() > 1) {
		h = build();
		HD(b) = h;
	}

	// add actual BBs and edges
	for(auto c: cbbs) {
		Block *nbb = build(c->insts.detach());
		BB(bb).add(pair(nbb, int(c->branch)));
		if(h != nullptr)
			build(h, nbb, 0);
		delete c;
	}

}


/**
 * Build a nop for the given instruction.
 * @param i		Instruction to build nop for.
 * @param c		Condition supported by the nop.
 */
Inst *ConditionalRestructurer::nop(Inst *i, const Condition& c) {
	if(_anop != i || !c.isEmpty()) {
		_nop = new GuardNOP(workspace(), i, c);
		_anop = i;
	}
	return _nop;
}


/**
 * Build a guarded instruction shield.
 * @param i		Guarded instruction.
 * @param cond	Condition guarding the instruction.
 * @return		Corresponding guarded instruction.
 */
Inst *ConditionalRestructurer::guard(Inst *i, const Condition& cond) {
	return new GuardInst(workspace(), i, cond);
}


/**
 * Build an instruction executing in its condition context (no need for
 * condition).
 * @param i		Condition instruction.
 * @return		Corresponding condition instruction.
 */
Inst *ConditionalRestructurer::cond(Inst *i) {
	return new CondInst(workspace(), i);
}


/**
 * Test if duplicating the BB at the given instruction with the given condition
 * is useful or not, i.e. if the condition will apply to several instructions
 * (in sequence).
 * @param i		Current instruction.
 * @param cond	Instruction condition.
 */
bool ConditionalRestructurer::useful(BasicBlock::InstIter i, const Condition& cond) {
	i++;
	if(i.ended())
		return false;
	auto nc = (*i)->condition();
	return !nc.isEmpty() && !nc.isAny() && nc.reg() == cond.reg();
}


/**
 * Build the corresponding blocks.
 * @param b		Block to rebuild.
 */
void ConditionalRestructurer::link(Block *b) {
	for(auto e: b->outEdges()) {
		for(auto sb: BB.all(b)) {
			if((e->isTaken() && ((sb.snd & TAKEN) != 0))			// taken and taken generated
			|| (e->isNotTaken() && ((sb.snd & NOT_TAKEN) != 0))	// not-taken and not-taken generated
			|| (!e->isTaken() && !e->isNotTaken()))
			{
				if(HD(e->sink()) != nullptr)
					build(sb.fst, HD(e->sink()), e->flags());
				else
					for(auto tb: BB.all(e->sink()))
						build(sb.fst, tb.fst, e->flags());
			}
		}
	}
}

} // otawa
