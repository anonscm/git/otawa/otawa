/*
 *	Processor class implementation
 *
 *	This file is part of OTAWA
 *	Copyright (c) 2013, IRIT UPS.
 *
 *	OTAWA is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	OTAWA is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OTAWA; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <elm/io/ansi.h>
#include <otawa/proc/Processor.h>
#include <otawa/proc/ProcessorException.h>
#include <elm/debug.h>

using namespace elm;

namespace otawa {

/**
 * @class ProcessorException
 * This class is used for returning exceptions from the processors.
 * @ingroup proc
 */

/**
 * Build a processor exception with a simple message.
 * @param proc		Processor throwing the exception.
 * @param message	Exception message.
 */
ProcessorException::ProcessorException(
	const Processor& proc,
	const elm::String& message
): Exception(_
	<< proc.name() << " (" << proc.version() << "): "
	<< message
) {
}

} // otawa
