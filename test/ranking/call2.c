
int f(int n) {
	int s = 0;
	for(int i = 0; i < n; i++)
		s += i;
	return s;
}

int main() {
	int s = 0;
	for(int i = 0; i < 10; i++) {
		s -= f(i-1);
		s += f(i+1);
	}
	return s;
}
