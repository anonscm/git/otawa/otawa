/*
 *	Test file for CFG features
 *
 *	This file is part of OTAWA
 *	Copyright (c) 2018, IRIT UPS.
 *
 *	OTAWA is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	OTAWA is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OTAWA; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <elm/test.h>
#include <otawa/sem/inst.h>

using namespace elm;
using namespace otawa;

int main() {
	const int
		sr = 16,
		r0 = 0,
		r1 = 1,
		r2 = 2,
		t1 = -1,
		t2 = -2;
	
	// block fork: no transformation
	{
		sem::Block b;
		b.add(sem::add(r0, r1, r1));
		b.add(sem::seti(t1, 2));
		b.add(sem::shl(r2, r0, t1));
		b.add(sem::cont());
		b.fork();
		b.print(cout);
		cout << io::endl << io::endl;
	}
	
	// block fork: simple fork
	{
		sem::Block b;
		b.add(sem::seti(t1, 0));
		b.add(sem::cmp(sr, r0, t1));
		b.add(sem::_if(sem::EQ, sr, 2));
		b.add(sem::seti(t2, 0x1000));
		b.add(sem::branch(t2));
		b.add(sem::cont());
		cout << "BEFORE:\n"; b.print(cout); cout << io::endl;
		b.fork(); 
		cout << "AFTER:\n"; b.print(cout); cout << io::endl << io::endl;
	}
	
}
